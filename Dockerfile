FROM node:18-alpine
WORKDIR /app
COPY package*.json ./
RUN npm install
RUN curl -sSL https://get.docker.com/ | sh
RUN apk add --update --no-cache openssh
COPY . .
EXPOSE 5000
RUN npx prisma generate
CMD [ "npm", "run", "dev", "ssh" ]